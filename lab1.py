import random
from time import time
from math import log


MIN_INT = -(2**31)
MAX_INT = 2**31 -1

N_MIN =  1000
N_MAX = 10000
STEP =   1000
REPS =    100


def make_data_set(n):
    arr = []
    for i in range(n):
        arr.append(random.randint(MIN_INT, MAX_INT))
    return arr

	
def alg1(arr):
    arr.sort()
    return arr[0], arr[-1]

	
def alg2(arr):
    minimum = maximum = arr[0]
    for i in range(1, len(arr)):
        if arr[i] < minimum:
    	    minimum = arr[i]
        if arr[i] > maximum:
            maximum = arr[i]
    return minimum, maximum

	
def alg3(arr):
    length = len(arr)
    if length%2 == 1:
        minimum = maximum = arr[0]
        for i in range(1, len(arr), 2):
            a = arr[i]
            b = arr[i+1]
            local_min, local_max = (a, b) if a < b else (b, a)
            if local_min < minimum:
                minimum = local_min
            if local_max > maximum:
                maximum = local_max
    else:
        a = arr[0]
        b = arr[1]
        minimum, maximum = (a, b) if a < b else (b, a)
        for i in range(2, len(arr), 2):
            a = arr[i]
            b = arr[i+1]
            local_min, local_max = (a, b) if a < b else (b, a)
            if local_min < minimum:
                minimum = local_min
            if local_max > maximum:
                maximum = local_max
    return minimum, maximum

	
def measure_time(fun, arr):	
    start = time()
    fun(arr)
    return time() - start

	
def repeat(fun, data_size, complexity, reps, time_multiplier = 10000000):
    time = 0
    for i in range(reps):
        arr = make_data_set(data_size)
        time += measure_time(fun, arr)
    return ((time * time_multiplier) / reps) / complexity[fun](data_size)


def test(functions, complexity, value_range, reps):
    for data_size in value_range:
        print("n={:d}\t".format(data_size), end="")
        for f in functions:
            time = repeat(f, data_size, complexity, reps)
            print(round(time, 3), "\t", end="")
        print()


expected_complexity = {alg1: lambda n: n * log(n, 2), alg2: lambda n: n, alg3: lambda n: n}

print("\tsort\tsimple\tpair")
print("\tnlog(n)\tn\tn")
print("-----------------------------")
test([alg1, alg2, alg3], expected_complexity, range(N_MIN, N_MAX+STEP, STEP), REPS)
	

	
	
			
	
	
