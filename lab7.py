import collections
from math import sqrt, floor
import random
from time import time
import matplotlib.pyplot as plt
import numpy as np

TIME_MUL = 10 ** 5
REPS = 10

Point = collections.namedtuple('Point', ['x', 'y'])


def draw_plot(points, nearest):
    n = len(points)
    plt.scatter([p.x for p in points], [p.y for p in points], s=10, c='black')
    plt.xticks(np.arange(0, 1 * n + 1, n / 10))
    plt.yticks(np.arange(0, 1 * n + 1, n / 10))
    plt.scatter([h.x for h in nearest], [h.y for h in nearest], s=30, c='yellow')
    plt.grid(True)
    plt.show()


def distance(p1, p2):
    return sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2)


def distance2(p1, p2):
    dist = sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2)
    return [dist, [p1, p2]]


def generate_points(n):
    xs = random.sample(range(1 * n), n)
    ys = random.sample(range(1 * n), n)
    return [Point(x, y) for x, y in zip(xs, ys)]


def compute_key(point, r):
    a = floor(point.x / r)
    b = floor(point.y / r)
    return a, b


def add_to_grid(grid, point, r):
    key = (compute_key(point, r))
    if key not in grid:
        grid[key] = [point]
    else:
        grid[key].append(point)
    return grid


def build_grid(points, r):
    grid = {}
    for point in points:
        add_to_grid(grid, point, r)
    return grid


def get_neighbourhood(grid, point, r):
    a, b = compute_key(point, r)
    neighbourhood = []
    for i in [(a - 1)*r, a*r, (a + 1)*r]:
        for j in [(b - 1)*r, b*r, (b + 1)*r]:
            key = (floor(i), floor(j))
            if key in grid:
                for p in grid[key]:
                    if p != point:
                        neighbourhood.append(p)
    return neighbourhood


def randomized_closest_pair(points):
    r = distance(points[0], points[1])
    p, q = points[0], points[1]
    grid = build_grid(points, r)
    for i in range(2, len(points)):
        distances = [distance2(points[i], p) for p in get_neighbourhood(grid, points[i], r)]
        if distances:
            d = min(distances)
            new_r = min(r, d[0])
            if new_r < r:
                p, q = d[1]
            elif new_r == r:
                add_to_grid(grid, points[i], new_r)
            else:
                grid = build_grid(points[0, i], new_r)
            r = new_r
    return (p, q), r


def measure_time(f, args):
    start = time()
    f(*args)
    return time() - start


def repeat(function, number_of_points, complexity):
    time = 0
    for i in range(REPS):
        points = generate_points(number_of_points)
        time += measure_time(function, (points,))
    return ((time * TIME_MUL) / REPS) / complexity(number_of_points)


if __name__ == '__main__':
    points = generate_points(100)
    points.append(Point(19, 71))
    points.append(Point(20, 71))
    random.shuffle(points)
    nearest, dist = randomized_closest_pair(points)
    print(dist)
    draw_plot(points, nearest)

    # for r in [range(100, 1000, 100), range(1000, 10000, 1000), range(10000, 60000, 10000)]:
    #     for n in r:
    #         print("n = {:5d}\tt = {:.2f}".format(n, repeat(randomized_closest_pair, n, lambda n: n)))


