import collections
import random
from math import sqrt
from time import time
from timeit import timeit

Point = collections.namedtuple('Point', ['x', 'y'])


# TODO: wczytywanie danych z pliku
def generate_points(n):
    xs = random.sample(range(1,10*n), n)
    ys = random.sample(range(1,10*n), n)
    return list(Point(x,y) for x,y in zip(xs, ys))

    
def init(points):
    Sx = sorted([p.x for p in points])
    Sy = sorted([p.y for p in points])
    return Sx, Sy


def divide(points, Sx, Sy):
    pivot = len(points)//2
    points1 = points[:pivot]
    points2 = points[pivot:]
    Sx1 = [x for x in Sx if x in [p.x for p in points1]]
    Sx2 = [x for x in Sx if x in [p.x for p in points2]]
    Sy1 = [y for y in Sy if y in [p.y for p in points1]]
    Sy2 = [y for y in Sy if y in [p.y for p in points2]]
    return points1, points2, Sx1, Sx2, Sy1, Sy2

def distance(p, q):
    return sqrt((q.x - p.x)**2 + (q.y - p.y)**2)
	
def measure_time(fun, arr):	
    start = time()
    fun(arr)
    return time() - start

	
def repeat(fun, data_size, complexity, reps, time_multiplier = 10000000):
    time = 0
    for i in range(reps):
        arr = make_data_set(data_size)
        time += measure_time(fun, arr)
    return ((time * time_multiplier) / reps) / complexity[fun](data_size)


def test(functions, complexity, value_range, reps):
    for data_size in value_range:
        print("n={:d}\t".format(data_size), end="")
        for f in functions:
            time = repeat(f, data_size, complexity, reps)
            print(round(time, 3), "\t", end="")
        print()


points = [Point(1,3), Point(2,5), Point(3,1), Point(4,2), Point(6,6), Point(9,4), Point(12,7)]
Sx, Sy = init(points)
points1, points2, Sx1, Sx2, Sy1, Sy2 = divide(points, Sx, Sy)
for var in (points1, points2, Sx1, Sx2, Sy1, Sy2):
    print(var)

#expected_complexity = {alg1: lambda n: n * log(n, 2), alg2: lambda n: n, alg3: lambda n: n}

#print("\tsort\tsimple\tpair")
#print("\tnlog(n)\tn\tn")
#print("-----------------------------")
#test([alg1, alg2, alg3], expected_complexity, range(N_MIN, N_MAX+STEP, STEP), REPS)
	

	
	
			
	
	
